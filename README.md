# yeoman_investigation

## Getting Started

### Environment Setup

* Build new container:

        docker build -rm -t centos/yeoman .

* Ensure that environment was built:

        docker images | grep centos/yeoman

* Get into environment:

        docker run -i -t centos/yeoman /bin/bash

* `sudo` to non-root user:

        sudo su - us

Remember that whatever changes you make inside the shell (like installing the yeoman generator-webapp) will be lost when you exit unless you commit a new container image first. This could be done with a command like:

    docker commit -author="aslag" container_id generator-angular

...where "container_id" can be found using the command `docker ps`

### Self-Guided Investigation Starting Point

Follow guide at <https://github.com/yeoman/yeoman/wiki/Getting-Started> to try yeoman. Note that all admin-level commands (`yum`, etc.) need to run with `sudo`; `npm`, `yo`, and other project-level commands can be run as the "us" user.

### Investigation Command Record
    sudo su - us

    npm install -g generator-angular

    yo angular

... I answered "no" to compass (it requires the ruby runtime) and "yes" to twitter bootstrap and angular's route library.

from centos
maintainer aslag <aslag@tehlulz.com>
run rpm -i http://dl.fedoraproject.org/pub/epel/6/i386/epel-release-6-8.noarch.rpm
#
# environment and user setup
#
run yum install --nogpgcheck -y yum-utils which tree git sudo && sed -iBAK 's/# \(%wheel.*NOPASSWD:.*\)/\1/' /etc/sudoers
run useradd -m -d /us -U -G wheel us
#
# project dir and ownership setup
#
add . /us/proj
run chown -R us:us /us/proj
workdir /us
run yum install --nogpgcheck -y nodejs npm make rpm-build
# we can be really liberal about this perm stuff b/c we're running containerized
run for dir in "/usr/lib" "/usr/local" "/usr/share"; do chgrp -R us $dir && find $dir -maxdepth 1 -type d -exec chmod g+r,g+w,g+x {} \;; done
run for dir in "/usr/local/share/man"; do chgrp -R us $dir && find $dir -type d -exec chmod g+r,g+w,g+x {} \;; done
run echo 'alias npm="command npm --prefix /usr/local"' >> /us/.bashrc
# why no worky?: run su - us -c ". ~/.bash_profile; npm install -g yo grunt-cli bower"
run su - us -c "npm install --prefix /usr/local -g yo grunt-cli bower"
